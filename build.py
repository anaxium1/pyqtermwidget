import os
import sys
import sipconfig
from PyQt5 import QtCore

build_file = "qtermwidget.sbf"

config = sipconfig.Configuration()

os.system(" ".join([config.sip_bin, "-c", ".", "-b", build_file,
    "-I", "/usr/share/sip/PyQt5", QtCore.PYQT_CONFIGURATION["sip_flags"],
    "qtermwidget.sip"]))

makefile = sipconfig.SIPModuleMakefile(config, build_file)

makefile.extra_include_dirs = [ "/usr/include/x86_64-linux-gnu/qt5/QtCore", "/usr/include/x86_64-linux-gnu/qt5", "/usr/include/x86_64-linux-gnu/qt5/QtGui", "/usr/include/qtermwidget5", "/usr/include/x86_64-linux-gnu/qt5/QtWidgets" ]
makefile.extra_libs = [ "qtermwidget5" ]

makefile.generate()
