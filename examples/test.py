from PyQt5 import QtWidgets
from QTermWidget import QTermWidget
import os


class Terminal(QTermWidget):
    def __init__(self):
        super().__init__(1)
        self.setColorScheme("Solarized")
        self.show()

app = QtWidgets.QApplication([])
term = Terminal()
app.exec()
